<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="">
  <link rel="icon" type="image/png" href="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>
    Dashboard
  </title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

  <!--  Social tags      -->
  <meta name="keywords" content="Artist, 7 Wonders Band, Band, Musicians">
  <meta name="description" content="A rock band situated in Thecho, Lalitpur. It is the first band of Thecho.">


  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="Admin Dashboard for 7 Wonders Band">
  <meta itemprop="description" content="">

  <meta itemprop="image" content="">


  <!-- Twitter Card data -->
  <meta name="twitter:card" content="7 Wonders Band">
  <meta name="twitter:site" content="@7_wonders_band">
  <meta name="twitter:title" content="Dashboard for 7 Wonders Band">

  <meta name="twitter:description" content="Simple Dashboard fro 7 Wonders Band designed for customer flexibility with bootstrap.">
  <meta name="twitter:creator" content="@suman_mali">
  <meta name="twitter:image" content="">

  <!--     Fonts and icons     -->

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Files -->

  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/now-ui-dashboard.min.css?v=1.5.0') }}" rel="stylesheet" />
  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery.min.js') }}" ></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}" ></script>
  <script src="{{ asset('assets/js/core/bootstrap.min.js') }}" ></script>

  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
  @yield('scripts')
</head>

<body class="">
  <div class="wrapper ">

    @include('backend.sidebar')
    <div class="main-panel" id="main-panel">
      @include('backend.navbar')
      <div class="panel-header panel-header-sm">

      </div>
      <div class="content">
        @include('layouts.flash')

        @yield('content')
      </div>

      @include('backend.footer')
    </div>
  </div>
  <div class="fixed-plugin">
    <div class="dropdown show-dropdown">
      <a href="#" data-toggle="dropdown">
        <i class="fa fa-cog fa-2x"></i>
      </a>
      <ul class="dropdown-menu">
       <li class="header-title">Sidebar Background</li>
       <li class="adjustments-line">
        <a href="javascript:void(0)" class="switch-trigger background-color">
          <div class="badge-colors text-center">
            <span class="badge filter badge-yellow" data-color="yellow"></span>
            <span class="badge filter badge-blue" data-color="blue"></span>
            <span class="badge filter badge-green" data-color="green"></span>
            <span class="badge filter badge-orange active" data-color="orange"></span>
            <span class="badge filter badge-red" data-color="red"></span>
          </div>
          <div class="clearfix"></div>
        </a>
      </li>
      <br>
    </ul>
  </div>
</div>
<script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js') }}" ></script>

<!-- Chart JS -->
<script src="{{ asset('assets/js/plugins/chartjs.min.js') }}"></script>

<!--  Notifications Plugin    -->
<script src="{{ asset('assets/js/plugins/bootstrap-notify.js') }}"></script>





<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc --><script src="{{ asset('assets/js/now-ui-dashboard.min.js?v=1.5.0') }}" type="text/javascript"></script>


<!-- Sharrre libray -->
<script src="{{ asset('assets/demo/jquery.sharrre.js') }}"></script>
  <script>
    $(document).ready(function(){
      $().ready(function(){
        $sidebar = $('.sidebar');
        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');
        sidebar_mini_active = true;

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        // if( window_width > 767 && fixed_plugin_open == 'Dashboard' ){
        //     if($('.fixed-plugin .dropdown').hasClass('show-dropdown')){
        //         $('.fixed-plugin .dropdown').addClass('show');
        //     }
        //
        // }

        $('.fixed-plugin a').click(function(event){
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if($(this).hasClass('switch-trigger')){
            if(event.stopPropagation){
              event.stopPropagation();
            }
            else if(window.event){
             window.event.cancelBubble = true;
           }
         }
       });

        $('.fixed-plugin .background-color span').click(function(){
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if($sidebar.length != 0){
            $sidebar.attr('data-color',new_color);
          }

          if($full_page.length != 0){
            $full_page.attr('filter-color',new_color);
          }

          if($sidebar_responsive.length != 0){
            $sidebar_responsive.attr('data-color',new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function(){
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if( $sidebar_img_container.length !=0 && $('.switch-sidebar-image input:checked').length != 0 ){
            $sidebar_img_container.fadeOut('fast', function(){
             $sidebar_img_container.css('background-image','url("' + new_image + '")');
             $sidebar_img_container.fadeIn('fast');
           });
          }

          if($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0 ) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function(){
             $full_page_background.css('background-image','url("' + new_image_full_page + '")');
             $full_page_background.fadeIn('fast');
           });
          }

          if( $('.switch-sidebar-image input:checked').length == 0 ){
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image','url("' + new_image + '")');
            $full_page_background.css('background-image','url("' + new_image_full_page + '")');
          }

          if($sidebar_responsive.length != 0){
            $sidebar_responsive.css('background-image','url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').on("switchChange.bootstrapSwitch", function(){
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if($input.is(':checked')){
            if($sidebar_img_container.length != 0){
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image','#');
            }

            if($full_page_background.length != 0){
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image','#');
            }

            background_image = true;
          } else {
            if($sidebar_img_container.length != 0){
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if($full_page_background.length != 0){
              $full_page.removeAttr('data-image','#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function(){
          var $btn = $(this);

          if(sidebar_mini_active == true){
            $('body').removeClass('sidebar-mini');
            sidebar_mini_active = false;
            nowuiDashboard.showSidebarMessage('Sidebar mini deactivated...');
          }else{
            $('body').addClass('sidebar-mini');
            sidebar_mini_active = true;
            nowuiDashboard.showSidebarMessage('Sidebar mini activated...');
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function(){
            window.dispatchEvent(new Event('resize'));
          },180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function(){
            clearInterval(simulateWindowResize);
          },1000);
        });
      });
});
</script>
</body>
</html>
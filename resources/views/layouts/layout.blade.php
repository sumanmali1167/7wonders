<!DOCTYPE html>
<html lang="en">
<head>
	<title>7 Wonders</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Mixtape template project">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/bootstrap-4.1.2/bootstrap.min.css') }}">
	<link href="{{ asset('frontend/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/plugins/OwlCarousel2-2.2.1/animate.css') }}">
	@yield('styles')
</head>
<body>
	<div class="super_container">
		@include('frontend.header')
		@yield('content')
		@include('frontend.footer')
	</div>
	<script src="{{ asset('frontend/js/jquery-3.2.1.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/css/bootstrap-4.1.2/popper.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/css/bootstrap-4.1.2/bootstrap.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/greensock/TweenMax.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/greensock/TimelineMax.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/scrollmagic/ScrollMagic.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/greensock/animation.gsap.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/greensock/ScrollToPlugin.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/easing/easing.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/progressbar/progressbar.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/parallax-js-master/parallax.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/jPlayer/jquery.jplayer.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/plugins/jPlayer/jplayer.playlist.min.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script src="{{ asset('frontend/js/custom.js') }}" type="ca43bfb40916211ca2f03466-text/javascript"></script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="ca43bfb40916211ca2f03466-text/javascript"></script>
	<script type="ca43bfb40916211ca2f03466-text/javascript">
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script>
	<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="ca43bfb40916211ca2f03466-|49" defer=""></script>
</body>
</html>
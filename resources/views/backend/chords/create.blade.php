@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-6">
		<form action="{{ route('chords.store') }}" method="POST">
			@csrf
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Add Chords</h4>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="song_id">Select Song</label>
						<select name="song_id" class="form-control">
							<option value="">Select the Song</option>
							@foreach($songs as $song)
								<option value="{{ $song->id }}">{{ $song->title }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="chords">Chords</label>
						<textarea name="chords" cols="30" rows="50" class="form-control"></textarea>
					</div>
				</div>
				<div class="card-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">List of Chords</h4> 
				<div>
					<a href="{{ route('chords.create') }}" class="btn">
						Add New Chords
					</a>
				</div>    
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table">
						<thead class="text-primary">                   
							<tr>
								<th>    
									S.N  
								</th>
								<th> 
									Song
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($songs as $song)
							<tr>
								<td>    
									{{ $loop->iteration }}
								</td>
								<td> 
									@foreach($chords as $chord)
										@if($chord->song_id == $song->id)
          									<button class="btn btn-primary" onclick="displayChords(<?php echo $chord->id; ?>)">
          										{{ $song->title }}
          									</button>
          								@else
          									<button class="btn btn-primary" disabled>
          										{{ $song->title }}
          									</button>
										@endif
									@endforeach
								</td>
								<td>
									
								</td>
							</tr>
							@endforeach  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="card" id="showChords">
		</div>
	</div>
</div>
<script>
	function displayChords(id){
		fetch('http://127.0.0.1:8000/chords/' + id)
		.then((res) => {
			res.json().then((data) => {
				$('#showChords').css('opacity', '1');
				$('#showChords').append('<pre>'+ data.data.chords +'</pre>');
			})
		})
		.catch((error) => {
			console.log(error.json())
		});
	}
</script>
@endsection
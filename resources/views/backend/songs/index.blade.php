@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">
					List of Song
				</h4>
				<div>
					<a href="{{ route('songs.create') }}" class="btn">
						Add New Song
					</a>
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table">
						<thead class="text-primary">                   
							<tr>
								<th>    
									S.N  
								</th>
								<th>
									Title   
								</th>
								<th>
									Artist
								</th>
								<th>
									Genre  
								</th>
								<th>   
									Beat  
								</th>
								<th>   
									Scale  
								</th>
								<th>   
									Level  
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($songs as $song)
							<tr>
								<td>    
									{{ $loop->iteration }}
								</td>
								<td> 
									{{ $song->title }}   
								</td>
								<td>
								@foreach($artists as $artist)
									@if($artist->id == $song->artist_id)
										{{ $artist->name }}
									@endif
								@endforeach 
								</td>
								<td>   
									{{ $song->genre }}  
								</td>
								<td>   
									{{ $song->beat }}  
								</td>
								<td>   
									{{ $song->scale }}  
								</td>
								<td>   
									{{ $song->level }}  
								</td>
							</tr>
							@endforeach  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Add New Song</h4>     
			</div>
			<div class="card-body">
				<form action="{{ route('songs.store') }}" method="POST" class="addForm" enctype="multipart/form-data">
					@csrf
					<div class="form-group col-md-4">
						<label for="title">Song Title</label>
						<input type="text" name="title" class="form-control" required>
					</div>
					<div class="form-group col-md-4">
						<label for="artist_id">Artist</label>
						<select name="artist_id" class="form-control" required>
							<option value="">Select Artist</option>
							@foreach($artists as $artist)
								<option value="{{ $artist->id }}">{{ $artist->name }}</option>	
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-4">
						<label for="genre">Genre</label>
						<input type="text" name="genre" class="form-control">
					</div>
					<div class="form-group col-md-4">
						<label for="scale">Scale</label>
						<input type="text" name="scale" class="form-control">
					</div>
					<div class="form-group col-md-4">
						<label for="beat">Beat</label>
						<input type="text" name="beat" class="form-control">
					</div>
					<div class="form-group col-md-4">
						<label for="strumming">Strumming</label>
						<input type="text" name="strumming" class="form-control">
					</div>
					<div class="form-group col-md-4">
						<label for="level">Level</label>
						<input type="text" name="level" class="form-control">
					</div>
					<div class="form-group col-md-4">
						<label for="audio">Upload Audio</label>
						<input type="file" name="audio" class="form-control" accept="audio/mp3, audio/wav, audio/aac"/>
					</div>

					<button type="submit" class="btn btn-block col-md-4">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	function popAddArtist(){
		$('#addArtist').modal();
	}

</script>
@endsection
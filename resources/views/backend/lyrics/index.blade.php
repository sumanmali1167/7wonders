@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">List of Lyrics</h4>
				<div>
					<a href="{{ route('lyrics.create') }}" class="btn">
						Add New Lyrics
					</a>
				</div>    
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table">
						<thead class="text-primary">                   
							<tr>
								<th>    
									S.N  
								</th>
								<th> 
									Song
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($songs as $song)
							<tr>
								<td>    
									{{ $loop->iteration }}
								</td>
								<td> 
									@foreach($lyrics as $lyric)
										@if($lyric->song_id == $song->id)
          									<button class="btn btn-primary" onclick="displayLyrics(<?php echo $lyric->id; ?>)">
          										{{ $song->title }}
          									</button>
          								@else
          									<button class="btn btn-primary" disabled>
          										{{ $song->title }}
          									</button>
										@endif
									@endforeach
								</td>
							</tr>
							@endforeach  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="card" id="showLyrics">

		</div>
	</div>
</div>
<script>
	function displayLyrics(id){
		fetch('http://127.0.0.1:8000/lyrics/' + id)
		.then((res) => {
			res.json().then((data) => {
				$('#showLyrics').css('opacity', '1');
				$('#showLyrics').append('<pre>'+ data.data.lyrics +'</pre>');
			})
		})
		.catch((error) => {
			console.log(error.json())
		});
	}
</script>
@endsection
<div class="sidebar" data-color="orange">
  <div class="logo">
    <a href="/" class="simple-text logo-normal">
      &nbsp;&nbsp;7 Wonders Band
    </a>
  </div>
  <div class="sidebar-wrapper" id="sidebar-wrapper">
    <ul class="nav"> 
      <li class="active "  >
        <a href="/home">

          <i class="now-ui-icons design_app"></i>

          <p>Dashboard</p>
        </a>
      </li>
      <li >
        <a href="{{ route('artists.index') }}">

          <i class="now-ui-icons users_single-02"></i>

          <p>Artists</p>
        </a>
      </li>
      <li >
        <a href="{{ route('songs.index') }}">

          <i class="now-ui-icons tech_headphones"></i>

          <p>Songs</p>
        </a>
      </li>
      <li >
        <a href="{{ route('lyrics.index') }}">

          <i class="now-ui-icons education_agenda-bookmark"></i>

          <p>Lyrics</p>
        </a>
      </li>
      <li >
        <a href="{{ route('chords.index') }}">

          <i class="now-ui-icons media-2_note-03"></i>

          <p>Chords</p>
        </a>
      </li>
      <li >
        <a href="#">

          <i class="now-ui-icons users_single-02"></i>

          <p>User Profile</p>
        </a>
      </li> 
    </ul>
  </div>
</div>
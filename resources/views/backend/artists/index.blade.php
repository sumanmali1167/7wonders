@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">List of Artists</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table">
						<thead class="text-primary">                   
							<tr>
								<th>    
									S.N  
								</th>
								<th> 
									Artist / Band  
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($artists as $artist)
							<tr>
								<td>    
									{{ $loop->iteration }}
								</td>
								<td> 
									{{ $artist->name }}   
								</td>
							</tr>
							@endforeach  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<form action="{{ route('artists.store') }}" method="POST">
			@csrf
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Add Artist</h4>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="name">Name of Artist</label>
						<input type="text" name="name" class="form-control" required>
					</div>
				</div>
				<div class="card-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
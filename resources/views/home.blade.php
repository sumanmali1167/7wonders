@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">( {{ $artists->count() }} ) <em>Artists</em></h5>
                <h4 class="card-title">All Artists</h4>
                <div class="dropdown">
                    <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                        <i class="now-ui-icons loader_gear"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('artists.index') }}">Add</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <ol>
                    @foreach($artists as $artist)
                        <li>{{ $artist->name }}</li>
                    @endforeach
                    </ol>
                </div>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">( {{ $songs->count() }} ) <em>Songs</em></h5>
                <h4 class="card-title">All Songs</h4>
                <div class="dropdown">
                    <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                        <i class="now-ui-icons loader_gear"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('songs.create') }}">Add Song</a>
                        <a class="dropdown-item" href="{{ route('chords.create') }}">Add Chords</a>
                        <a class="dropdown-item" href="{{ route('lyrics.create') }}">Add Lyrics</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <ol>
                        @foreach($songs as $song)
                            <li>{{ $song->title }}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">( {{ $users->count() }} ) <em>Users</em></h5>
                <h4 class="card-title">All Users</h4>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <ol>
                        @foreach($users as $user)
                            <li>{{ $user->name }}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="now-ui-icons ui-2_time-alarm"></i> Last 7 days
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
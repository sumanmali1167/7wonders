<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Chords extends Model
{
    protected $fillable = [
    	'chords', 'song_id'
    ];
}

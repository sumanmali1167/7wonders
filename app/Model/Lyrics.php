<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lyrics extends Model
{
    protected $fillable = [
    	'song_id', 'lyrics'
    ];
}

<?php

namespace App\Http\Services;

class FileSavingService{
	/**
	 *Save file on the specified location.
	 * @param type $file 
	 * @param type|string $path 
	 * @return type 
	 */
	public function saveFile($file, $title, $path){
		$filename = $title . time() . $file->getClientOriginalExtension();
		$location = public_path('storage/'.$path);
		$file->move($location, $filename);
		return $filename;
	}
}
<?php

namespace App\Http\Controllers;

use App\Model\Song;
use App\Model\Artist;
use Illuminate\Http\Request;
use App\Http\Requests\Song\createRequestSong;
use App\Repositories\SongRepository;

class SongController extends Controller
{
    protected $songRepo;

    public function __construct(SongRepository $repo){
        $this->songRepo = $repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = Song::all();
        $artists = Artist::orderBy('name')->get();
        return view('backend.songs.index', compact('songs', 'artists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $artists = Artist::orderBy('name')->get();
        return view('backend.songs.create', compact('artists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createRequestSong $request)
    {
        $song = $this->songRepo->create($request);
        return redirect()->back()->with('success', 'A new song is added successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function show(Song $song)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function edit(Song $song)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Song $song)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function destroy(Song $song)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Model\Lyrics;
use App\Model\Song;
use Illuminate\Http\Request;

class LyricsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = Song::all();
        $lyrics = Lyrics::all();
        return view('backend.lyrics.index', compact('songs', 'lyrics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $songs = Song::all();
        return view('backend.lyrics.create', compact('songs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Lyrics::create($request->all());
        return redirect('/lyrics')->with('success', 'Lyrics is added successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Lyrics  $lyrics
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lyric = Lyrics::findOrFail($id);
        return response()->json(['data' => $lyric]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Lyrics  $lyrics
     * @return \Illuminate\Http\Response
     */
    public function edit(Lyrics $lyrics)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Lyrics  $lyrics
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lyrics $lyrics)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Lyrics  $lyrics
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lyrics $lyrics)
    {
        //
    }
}

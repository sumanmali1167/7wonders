<?php

namespace App\Http\Controllers;

use App\Model\Chords;
use App\Model\Song;
use Illuminate\Http\Request;

class ChordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chords = Chords::all();
        $songs = Song::all();
        return view('backend.chords.index', compact('chords', 'songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $songs = Song::all();
        return view('backend.chords.create', compact('songs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Chords::create($request->all());
        return redirect('/chords')->with('success', 'Chords is added successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Chords  $chords
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chord = Chords::findOrFail($id);
        return response()->json(['data' => $chord]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Chords  $chords
     * @return \Illuminate\Http\Response
     */
    public function edit(Chords $chords)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Chords  $chords
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chords $chords)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Chords  $chords
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chords $chords)
    {
        //
    }
}

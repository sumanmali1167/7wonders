<?php

namespace App\Http\Requests\Song;

use Illuminate\Foundation\Http\FormRequest;

class createRequestSong extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'artist_id' => 'required',
            'genre' => 'nullable',
            'scale' => 'required',
            'beat' => 'nullable',
            'strumming' => 'nullable',
            'level' => 'nullable',
            'audio' => 'nullable|file|mimes:audio/mp3,wav,aac'
        ];
    }
}

<?php

namespace App\Repositories;
use App\Http\Services\FileSavingService;
use App\Model\Song;

class SongRepository{

	protected $fileService ;

	public function __construct(FileSavingService $file){
		$this->fileService = $file;
	}

	public function create($request){
		$input = $request->all();
		if(file_exists($request->audio)){
			$input['audio'] = $this->fileService->saveFile($request->file('audio'), 'audio', 'audio');
		}
		$song = Song::create($input);
		return $song;
	}

	public function update($request, $song){
		$input = $request->all();
		if(file_exists($request->audio)){
			$input['audio'] = $this->fileService->saveFile($request->file('audio'), 'audio', 'audio');
		}
		else{
			$input['audio'] = $song->audio;
		}
		return $song->update($input);
	}
}
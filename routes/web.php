<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.homepage');
});
Route::get('/about', function () {
    return view('frontend.innerpages.about');
});
Route::get('/blog', function () {
    return view('frontend.innerpages.blog');
});
Route::get('/contact', function () {
    return view('frontend.innerpages.contact');
});
Auth::routes();

Route::middleware('auth')->group(function () {
	Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('songs', 'SongController');
    Route::resource('artists', 'ArtistController');
    Route::resource('lyrics', 'LyricsController');
    Route::resource('chords', 'ChordsController');
});